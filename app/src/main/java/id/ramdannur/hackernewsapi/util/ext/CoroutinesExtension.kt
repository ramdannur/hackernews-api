package id.ramdannur.hackernewsapi.util.ext

import kotlinx.coroutines.*

//https://medium.com/ta-tonthongkam/migrate-your-android-project-to-kotlin-1-3-7cfd32246f58

fun launchAsync(block: suspend CoroutineScope.() -> Unit): Job {
    return GlobalScope.launch(Dispatchers.Main) { block() }
}

suspend fun <T> async(block: suspend CoroutineScope.() -> T): Deferred<T> {
    return GlobalScope.async(Dispatchers.IO) { block() }
}

suspend fun <T> asyncAwait(block: suspend CoroutineScope.() -> T): T {
    return async(block).await()
}
package id.ramdannur.hackernewsapi.util

/**
 * Created by Ramdannur on 28/11/2019.
 */

class ViewConstants {
    companion object {
        const val SOCCER_TYPE: String = "Soccer"
        const val ITEM_KEY_EXTRA: String = "item"
        const val REQUEST_APP_PERMISSIONS = 1001
        const val BOTTOM_NAVIGATION_MENU_INDEX = "bottomNavigationMenuIndex"

        const val PAGE_LAST_MATCH: Int = 0
        const val PAGE_NEXT_MATCH: Int = 1
    }
}
package id.ramdannur.hackernewsapi.ui.base

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import id.ramdannur.hackernewsapi.R

/**
 * Created by Ramdannur on 10/17/2019.
 */

abstract class BaseActivity : AppCompatActivity() {

    fun setToolbarHome(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home_vector)
        }
    }

    fun setToolbarBack(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        }
    }

}

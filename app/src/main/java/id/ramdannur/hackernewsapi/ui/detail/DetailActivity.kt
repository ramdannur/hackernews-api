package id.ramdannur.hackernewsapi.ui.detail

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import id.ramdannur.hackernewsapi.R
import id.ramdannur.hackernewsapi.model.Comment
import id.ramdannur.hackernewsapi.ui.base.BaseActivity
import id.ramdannur.hackernewsapi.util.ViewConstants
import id.ramdannur.hackernewsapi.util.ext.gone
import id.ramdannur.hackernewsapi.util.ext.visible
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.architecture.ext.viewModel
import java.text.SimpleDateFormat
import java.util.*


class DetailActivity : BaseActivity() {

    private val viewModel: DetailViewModel by viewModel()
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private val list: ArrayList<Comment> = arrayListOf()

    companion object {
        fun buildIntent(
            activity: BaseActivity,
            id: Int
        ): Intent {
            return Intent(activity, DetailActivity::class.java)
                .putExtra(ViewConstants.ITEM_KEY_EXTRA, id)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val id = intent.getIntExtra(ViewConstants.ITEM_KEY_EXTRA, 0)

        viewModel.start()

        init()

        setupObservers()

        if (id != 0) viewModel.getData(id)
    }

    private fun init() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.story_detail)

        val adapter = CommentAdapter(list) {}
        recyclerview.isNestedScrollingEnabled = false
        recyclerview.adapter = adapter

        favoriteState()
    }

    private fun setupObservers() {
        //Current currency
        viewModel.item.observe(this, Observer {
            it?.let {
                tv_title.text = it.title
                tv_by.text = "By ${it.by}"
                tv_url.text = it.url

                val mydate = Calendar.getInstance()
                mydate.timeInMillis = it.time * 1000

                val formatter = SimpleDateFormat("dd/MM/yyyy")
                tv_time.text = formatter.format(mydate)
            }
//            showNoDataFound(adapter.dataList.isEmpty())
        })

        viewModel.comments.observe(this, Observer { newList ->
            if (newList != null && newList.isNotEmpty()) {
                list.clear()
                list.addAll(newList)
            }
        })

        //ProgressBar
        viewModel.isDataLoading.observe(this, Observer {
            if (it == true) {
                recyclerview.gone()
                progressBar.visible()
            } else {
                progressBar.gone()
                recyclerview.visible()
            }
        })
    }

    private fun addToFavorite() {
        // Not implemented yet
    }

    private fun removeFromFavorite() {
        // Not implemented yet
    }

    private fun favoriteState() {
        // Not implemented yet
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_favorite, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()

                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}

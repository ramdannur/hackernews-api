package id.ramdannur.hackernewsapi.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

/**
 * Created by Ramdannur on 10/17/2019.
 */
abstract class BaseFragment : Fragment() {

    // the root view
    private var rootView: View? = null

    protected abstract val layout: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = if (view != null)
            view
        else
            inflater.inflate(layout, container, false)
        return rootView
    }

}

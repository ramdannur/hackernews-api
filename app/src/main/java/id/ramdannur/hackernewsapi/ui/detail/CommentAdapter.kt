package id.ramdannur.hackernewsapi.ui.detail

import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.ramdannur.hackernewsapi.R
import id.ramdannur.hackernewsapi.model.Comment
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentAdapter(
    private val dataList: ArrayList<Comment>,
    private val listener: (Comment) -> Unit
) : RecyclerView.Adapter<CommentAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList[position], listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            comment: Comment,
            listener: (Comment) -> Unit
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                itemView.tv_title.text = Html.fromHtml(comment.Text, Html.FROM_HTML_MODE_COMPACT)
            } else {
                itemView.tv_title.text = Html.fromHtml(comment.Text)
            }

            itemView.tv_by.text = "By ${comment.By}"

            itemView.setOnClickListener {
                listener(comment)
            }
        }
    }
}
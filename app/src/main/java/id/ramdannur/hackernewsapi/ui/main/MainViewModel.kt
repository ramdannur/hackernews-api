package id.ramdannur.hackernewsapi.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import id.ramdannur.hackernewsapi.data.WebRepository
import id.ramdannur.hackernewsapi.data.remote.dto.Dto
import id.ramdannur.hackernewsapi.model.Article
import id.ramdannur.hackernewsapi.ui.base.BaseViewModel
import id.ramdannur.hackernewsapi.util.ext.launchAsync

class MainViewModel(private val repository: WebRepository) : BaseViewModel() {

    private val _articles: MutableLiveData<List<Article>> = MutableLiveData()
    val articles: LiveData<List<Article>>
        get() = _articles

    private val max: Int = 20 // max show data

    internal fun getData() {
        val itemList: MutableList<Article> = ArrayList()
        launchAsync {
            try {
                //The events is loading
                setLoading()
                val dto = repository.getArticles()

                var i = 0
                var model: Dto
                for (articleId in dto) {
                    i++
                    model = repository.getArticleDetail(articleId)
                    itemList.add(
                        Article(
                            articleId,
                            model.title.toString(),
                            model.url.toString(),
                            model.score,
                            model.kids?.size ?: 0
                        )
                    )
                    if (i == max) break
                }

                //Request with a suspended repository funcion
                _articles.value = itemList
            } catch (t: Throwable) {
                //An error was throw
                setError(t)
                _articles.value = emptyList()
            } finally {
                //Isn't loading anymore
                setLoading(false)
            }
        }
    }

    internal fun start() {
        _articles.value = emptyList()
    }
}

package id.ramdannur.hackernewsapi.ui.main

import android.os.Bundle
import androidx.lifecycle.Observer
import id.ramdannur.hackernewsapi.R
import id.ramdannur.hackernewsapi.model.Article
import id.ramdannur.hackernewsapi.ui.base.BaseActivity
import id.ramdannur.hackernewsapi.ui.detail.DetailActivity
import id.ramdannur.hackernewsapi.util.ext.gone
import id.ramdannur.hackernewsapi.util.ext.visible
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.architecture.ext.viewModel

class MainActivity : BaseActivity() {

    private val viewModel: MainViewModel by viewModel()
    private val list: ArrayList<Article> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.start()

        init()

        setupObservers()

        viewModel.getData()
    }

    private fun init() {
        supportActionBar?.title = getString(R.string.top_stories)

        val adapter = MainAdapter(list) { article ->
            startActivity(DetailActivity.buildIntent(this, article.id))
        }

        recyclerview.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.articles.observe(this, Observer { newList ->
            list.clear()
            if (newList != null) list.addAll(newList)
        })

        //ProgressBar
        viewModel.isDataLoading.observe(this, Observer {
            if (it == true) {
                recyclerview.gone()
                progressBar.visible()
            } else {
                progressBar.gone()
                recyclerview.visible()
            }
        })
    }
}

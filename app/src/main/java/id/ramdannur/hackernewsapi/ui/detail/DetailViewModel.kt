package id.ramdannur.hackernewsapi.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import id.ramdannur.hackernewsapi.data.WebRepository
import id.ramdannur.hackernewsapi.data.remote.dto.Dto
import id.ramdannur.hackernewsapi.model.Comment
import id.ramdannur.hackernewsapi.ui.base.BaseViewModel
import id.ramdannur.hackernewsapi.util.ext.launchAsync

/**
 * Created by Ramdannur on 28/11/2019.
 */
class DetailViewModel(private val repository: WebRepository) : BaseViewModel() {
    private val _item: MutableLiveData<Dto> = MutableLiveData()
    val item: LiveData<Dto>
        get() = _item

    private val _comments: MutableLiveData<List<Comment>> = MutableLiveData()
    val comments: LiveData<List<Comment>>
        get() = _comments

    internal fun getData(id: Int) {
        val itemList: MutableList<Comment> = ArrayList()
        launchAsync {
            try {
                setLoading()

                val dto = repository.getArticleDetail(id)
                var i = 0
                var model: Dto

                // Get Comments
                dto.kids?.forEach { commentId ->
                    i++
                    model = repository.getCommentDetail(commentId)
                    itemList.add(Comment(commentId, model.text.toString(), model.by.toString()))
                }

                _comments.value = itemList
                _item.value = dto
            } catch (t: Throwable) {
                //An error was throw
                setError(t)
                _item.value = null
                _comments.value = emptyList()
            } finally {
                //Isn't loading anymore
                setLoading(false)
            }
        }
    }

    internal fun start() {
        _item.value = null
        _comments.value = emptyList()
        setLoading(false)
    }
}
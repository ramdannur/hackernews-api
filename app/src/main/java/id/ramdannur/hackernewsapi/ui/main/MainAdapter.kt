package id.ramdannur.hackernewsapi.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.ramdannur.hackernewsapi.R
import id.ramdannur.hackernewsapi.model.Article
import kotlinx.android.synthetic.main.item_article.view.*

class MainAdapter(
    private val dataList: ArrayList<Article>,
    private val listener: (Article) -> Unit
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList[position], listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_article, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            article: Article,
            listener: (Article) -> Unit
        ) {
            itemView.tv_title.text = article.Title
            itemView.tv_sub_title.text = article.Url
            itemView.tv_score.text = "Score : ${article.Score}"
            itemView.tv_comment.text = "Comment : ${article.Comment}"

            itemView.setOnClickListener {
                listener(article)
            }
        }
    }
}
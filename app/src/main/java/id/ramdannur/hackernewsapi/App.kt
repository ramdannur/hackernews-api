package id.ramdannur.hackernewsapi

import android.app.Application
import id.ramdannur.hackernewsapi.injection.module.remoteDatasourceModule
import id.ramdannur.hackernewsapi.injection.module.repositoryModule
import id.ramdannur.hackernewsapi.injection.module.viewModelModule
import org.koin.android.ext.android.startKoin

/**
 * Created by Ramdannur on 28/11/2019.
 */

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin(
            this, listOf(
                // inject module
                remoteDatasourceModule,
                repositoryModule,
                viewModelModule
            )
        )
    }
}
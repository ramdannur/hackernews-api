package id.ramdannur.hackernewsapi.injection.module

import id.ramdannur.hackernewsapi.data.WebRepository
import org.koin.dsl.module.applicationContext

/**
 * Created by Ramdannur on 28/11/2019.
 */

val repositoryModule = applicationContext {
    factory { WebRepository(get()) }
}
package id.ramdannur.hackernewsapi.injection.module

import com.google.gson.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import id.ramdannur.hackernewsapi.BuildConfig
import id.ramdannur.hackernewsapi.data.remote.endpoint.WebService
import id.ramdannur.hackernewsapi.util.RequestInterceptor
import id.ramdannur.hackernewsapi.util.UnsafeOkHttpClient
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Ramdannur on 28/11/2019.
 */

val remoteDatasourceModule = applicationContext {

    //RequestInterceptor
    bean { provideRequestInterceptor() }

    //LoggingInterceptop
    bean { provideLoggingInterceptor() }

    // provided web components
    bean { provideOkHttpClient(get(), get()) }

    bean { provideGson() }

    bean { provideRemoteDataSource(get(), get()) }
}

/**
 * Prove o parser de Json para a aplicação
 */
fun provideGson(): Gson {
    val builder = GsonBuilder()

    builder.registerTypeAdapter(Date::class.java, JsonDeserializer<Date> { json, _, _ ->
        json?.asJsonPrimitive?.asLong?.let {
            return@JsonDeserializer Date(it)
        }
    })

    builder.registerTypeAdapter(Date::class.java, JsonSerializer<Date> { date, _, _ ->
        JsonPrimitive(date.time)
    })

    return builder.create()
}


/**
 * Prove o interceptor das requisições. Utilizado para adicionar header de token, por exemplo.
 */
fun provideRequestInterceptor(): RequestInterceptor {
    return RequestInterceptor()
}

/**
 * Provê o interceptor de logging das requisições
 */
fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    //Adiciona log às requisições
    val logInterceptor = HttpLoggingInterceptor()
    logInterceptor.level = HttpLoggingInterceptor.Level.BODY

    return logInterceptor
}

/**
 * Provê o httpClient padrão para o App
 */
fun provideOkHttpClient(
    requestInterceptor: RequestInterceptor,
    logInterceptor: HttpLoggingInterceptor
): OkHttpClient {

    val builder = UnsafeOkHttpClient.getUnsafeOkHttpClient()

    //Adiciona os interceptors
    builder.addInterceptor(logInterceptor)
    builder.addInterceptor(requestInterceptor)

    builder.connectTimeout(2, TimeUnit.MINUTES)
    builder.readTimeout(1, TimeUnit.MINUTES)
    builder.readTimeout(1, TimeUnit.MINUTES)

    return builder.build()
}

/**
 * Provê o endpoint service para a aplicação
 */
fun provideRemoteDataSource(okHttpClient: OkHttpClient, gson: Gson): WebService {
    return Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
        .create(WebService::class.java)
}

package id.ramdannur.hackernewsapi.injection.module

import id.ramdannur.hackernewsapi.ui.detail.DetailViewModel
import id.ramdannur.hackernewsapi.ui.main.MainViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

/**
 * Created by Ramdannur on 28/11/2019.
 */

val viewModelModule = applicationContext {
    viewModel { MainViewModel(get()) }

    viewModel { DetailViewModel(get()) }
}
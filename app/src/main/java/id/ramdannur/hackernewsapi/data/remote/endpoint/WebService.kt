package id.ramdannur.hackernewsapi.data.remote.endpoint

import id.ramdannur.hackernewsapi.data.remote.dto.Dto
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Ramdannur on 28/11/2019.
 */


interface WebService {
    @GET("topstories.json?print=pretty")
    fun getTopStoriesAsync(): Deferred<List<Int>>

    @GET("item/{itemId}.json?print=pretty")
    fun getItemAsync(@Path("itemId") id: String): Deferred<Dto>
}
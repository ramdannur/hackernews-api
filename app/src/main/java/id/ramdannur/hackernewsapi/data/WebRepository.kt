package id.ramdannur.hackernewsapi.data

import id.ramdannur.hackernewsapi.data.remote.dto.Dto
import id.ramdannur.hackernewsapi.data.remote.endpoint.WebService

/**
 * Created by Ramdannur on 28/11/2019.
 */

class WebRepository(private val remoteDataSource: WebService) {

    suspend fun getArticles(): List<Int> {
        return remoteDataSource.getTopStoriesAsync().await()
    }

    suspend fun getCommentDetail(id: Int): Dto {
        return remoteDataSource.getItemAsync(id.toString()).await()
    }

    suspend fun getArticleDetail(id: Int): Dto {
        return remoteDataSource.getItemAsync(id.toString()).await()
    }
}
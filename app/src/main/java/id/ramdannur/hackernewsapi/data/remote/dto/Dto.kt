package id.ramdannur.hackernewsapi.data.remote.dto

data class Dto(
    val score: Int = 0,
    val by: String? = null,
    val id: Int? = null,
    val parent: Int? = null,
    val time: Long = 0,
    val title: String? = null,
    val text: String? = null,
    val type: String? = null,
    val descendants: Int? = null,
    val url: String? = null,
    val kids: List<Int>? = null
)

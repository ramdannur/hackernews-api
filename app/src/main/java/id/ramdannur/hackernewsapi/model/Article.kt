package id.ramdannur.hackernewsapi.model

/**
 * Created by Ramdannur on 11/28/2019.
 */

class Article(var id: Int, var Title: String, var Url: String, var Score: Int, var Comment: Int)